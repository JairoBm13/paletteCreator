function randomPalette(){
    var initialColor = Math.floor(Math.random()*360);
    var cantColors = 5;
    var increment = 360/cantColors;
    for(var i = 0; i<5; i++){
        var color = (initialColor+(increment*i))%360;
        //console.log($('#color'+(i+1)));
        console.log(color);
        $('#color'+(i+1)).css("background-color",'hsl('+color+', 100%, 50%)');
    }
    generateRules();
}

function generateRules(){
    var colors =[];
    $('.color-view').each(function(){
        var color = $(this).css('background-color');
        colors.push(color);
        console.log(color);
    });

    $('#css-rules').text(".website-background{ color: "+colors[0]+";}\n\n"
    +
    ".element-text{ color: "+colors[1]+";}\n\n"
    +
    ".element-border{ border-color: "+colors[2]+";}\n\n"
    +
   " .element-background{ background-color: "+colors[3]+";}\n\n"
    +
    ".header{ color: "+colors[4]+";}\n\n");
}

function clearCssRules(){
    console.log("ho")
    $('.color-view').each(function(){
        $(this).css('background-color', '#FFFFFF');
    });
    $('#css-rules').text("\n.website-background{ color: #FFFFFF;}\n\n"
    +
    ".element-text{ color: #FFFFFF;}\n\n"
    +
    ".element-border{ border-color: #FFFFFF;}\n\n"
    +
   " .element-background{ background-color: #FFFFFF;}\n\n"
    +
    ".header{ color: #FFFFFF;}\n\n");
}